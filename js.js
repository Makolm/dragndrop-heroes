var drag_control = {
    element: null,
    rect: null,
    shift_top: null,
    shift_left: null,
    x: null,
    y: null,

    set_element_rect: function(element, rect) {
        this.element = element;
        this.rect = rect;
    },

    set_shifts: function(x, y) {
        this.shift_left = x - this.rect.x;
        this.shift_top = y - this.rect.y;
    },

    clear: function() {
        this.element = null;
        this.rect = null;
        this.shift_top = null;
        this.shift_left = null;
        this.x = null;
        this.y = null;
    }
}

document.onmousedown = (event) => {
    if (event.which != 1) return;
    let drag_elem = event.target.closest('.draggable');

    if (!drag_elem) return;

    event.preventDefault();
    drag_elem.ondragstart = () => { return false; } // отменить выделение

    drag_control.set_element_rect(drag_elem, drag_elem.getBoundingClientRect());
    drag_control.set_shifts(event.clientX, event.clientY);

    drag_control.element.style.position = 'fixed';
    move_at(event.clientX - drag_control.shift_left, event.clientY - drag_control.shift_top, drag_control.element);
}

function move_at(x, y, element) {
    element.style.left = x + 'px';
    element.style.top = y + 'px';
}

document.onmousemove = (event) => {
    if (drag_control.element) {
        drag_control.x = event.clientX - drag_control.shift_left;
        drag_control.y = event.clientY - drag_control.shift_top;

        if (drag_control.x < 0)
            drag_control.x = 0;
        else if (drag_control.x >= document.documentElement.clientWidth - drag_control.element.offsetWidth)
            drag_control.x = document.documentElement.clientWidth - drag_control.element.offsetWidth;

        if (drag_control.y < 0) {
            drag_control.y = 0;
            if (window.scrollY != 0) {
                let pos_y = (window.scrollY >= 10) ? 10 : window.scrollY;
                window.scrollBy(0, -pos_y);
            }
        }

        if (window.innerHeight < (drag_control.y + drag_control.element.offsetHeight)) {
            drag_control.y = window.innerHeight - drag_control.element.offsetHeight;

            if (window.scrollY + window.innerHeight < document.documentElement.scrollHeight) {
                let result = window.scrollY + window.innerHeight + 10 - document.documentElement.scrollHeight;
                let pos_y = (result < 0) ? 10 : result;
                window.scrollBy(0, pos_y);
            }

        }

        move_at(drag_control.x, drag_control.y, drag_control.element);
    }
}

document.onmouseup = () => {
    if (drag_control.element) {
        drag_control.element.style.position = 'absolute';
        move_at(drag_control.x, drag_control.y + window.scrollY, drag_control.element);
        drag_control.element = null;
        drag_control.rect = null;
    }
}